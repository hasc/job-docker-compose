# Docker Compose setup for Abc Job Server
This repository contains a Docker Compose setup for running a job server, a reply consumer, and one worker, using RabbitMQ as broker.

## Getting started

```bash
docker-compose up
```

Please have a look at the API documentation at <http://localhost:8080/doc>

**Available Services:**

* Job API <http://localhost:8080/job>
* RabbitMQ Management Dashboard: <http://localhost:15672>
* RabbitMQ: <http://localhost:5672>
* MySQL <http://localhost:3306>

## Creating jobs

There is a test job available with the name `test`. You can use it to process jobs, sequences and batches. This jobs behaves in a certain way depending on the json input provided with the job.

* `sendOutput`: Some output that will be returned while processing (should be used in combination wil `sleep`)
* `exception`: An exception message that will be thrown when the job is processed
* `sleep`: number of seconds to sleep
* `output`: some output returned when the job completes
* `fail`: boolean, whether to terminate the job with status "failed" or "complete"

### Examples

Sent a POST request to http://localhost/job with once of the following request bodies

1. Create a job will be created that sleeps for 1 second.

	```json
	{
		"type":"Job",
		"name":"test",
		"input":"{\"sleep\":1}"
	}
	```

2. Create a Sequence (processed sequentially) consisting of two jobs.

	```json
	{
		"type": "Sequence",
		"children": [
			{
				"type": "Job",
				"name": "test"
			},
			{
				"type": "Job",
				"name": "test"
			}
		]
	}
	```

3. Create a Batch (processed in parallel) consisting of two jobs.

	```json
	{
		"type": "Sequence",
		"children": [
			{
				"type": "Job",
				"name": "test"
			},
			{
				"type": "Job",
				"name": "test"
			}
		]
	}
	```

## ToDo
* add scheduler
